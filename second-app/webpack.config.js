const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const path = require('path')

// const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development'
let target = "web";
let mode = "development";

console.log(
  '\n\n\nprocess.env.NODE_ENV',
  process.env.NODE_ENV
);

if (process.env.NODE_ENV === "production") {
  mode = 'production';
  target = 'browserslist'
}

module.exports = {
  // mode: 'development',
  // mode: 'production',
  mode: mode,
  target: target,

  // optional
  context: path.join(__dirname, 'src'),
  entry: './index.js',
  // entry: "./src/index.ts",

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public'),

    // nie do type: "asset/resource",
    // assetModuleFilename: "assets/images/[hash][ext][query]"

    // do type: "asset/resource",
    // assetModuleFilename: "images/[hash][ext][query]"

  },  

  resolve: {
    extensions: ['.ts', '.js']
  }, 

  plugins: [
    new HtmlWebpackPlugin({
      template: 'index.html',
      inject: false,
    }),
    new MiniCssExtractPlugin(),
  ],

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          // without additional loader it will reference babelrc
          loader: 'babel-loader'
        }
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: {
          loader: 'ts-loader'
        }
      },
      {
        test: /\.scss$/i,
        // use: ['style-loader', 'css-loader', 'sass-loader']
        use: [
          // MiniCssExtractPlugin.loader,
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // publicPath: 'data/[name].[ext]',
              publicPath: '[name].[ext]',
            }
          },
          'css-loader',
          'sass-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif)$/i,

        // type: "asset/inline",
        // type: "asset/resource",  // nic nie działa
        // type: "asset/source",
        // type: "asset",           // nie działa serve, nie działą build
        type: 'javascript/auto', // dziala, dziala ale nie daje zdjec przy buildzie do osobnego foldera

        use: [
          {
            loader: 'url-loader',

            options: {
              // limit: 8192,
              // name: '[path][name].[ext]',
            }
          },
        ],
      }
    ]
  },
  devtool: 'source-map',

  devServer: {
    // contentBase: './src'
    contentBase: './public'
  },
}