import './styles.scss'
import s2 from './../assets/img/s2.png';
import s1 from './../assets/img/s1.png';

const obj1 = { a: 'a', b: 'b'}
const obj2 = { ...obj1, c: 'c'}

console.log('obj2', obj2)
console.log('s1', s1); 
console.log('s2', s2);

let carElem = document.querySelector('.car-img');
let btn = document.querySelector('.car-btn');
btn.addEventListener('click', () => {
  carElem.classList.remove('hide')
  carElem.insertAdjacentHTML('afterend', `<div><img src="${s2}" width="200" height="200"></img></div>`)
})
